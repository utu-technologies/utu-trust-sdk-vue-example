# UTU Trust SDK Vue example app


UTU’s Trust API seamlessly serves up personalized recommendations for trusted service providers on sharing platforms to drive your conversion, satisfaction, retention, and viral acquisition.

[More information about UTU](https://utu.io)

UTU Trust SDK provides web-components that access UTU’s Trust API and allows you simple integration with its services.

Find the [repository here](https://bitbucket.org/utu-technologies/utu-trust-sdk)


## Features

- Integration with UTU's Trust API


## Examples

You can find other working examples in the links below 


[react example](https://bitbucket.org/utu-technologies/utu-trust-sdk-react-example/src/master/)

[angular example](https://bitbucket.org/utu-technologies/utu-sdk-angular-example-app/src/master/)

[vanilla javascript example](https://bitbucket.org/utu-technologies/utu-sdk-vanilla-js-example-app/src/master/)



Before running examples, run ```npm install``` in root folder then run ```npm install``` ,


```js
// vue.config.js
module.exports = {
  chainWebpack: config => {
    config.module
      .rule('vue')
      .use('vue-loader')
      .tap(options => {
        return {
          ...options,
          compilerOptions: {
            ...(options.compilerOptions || {}),
            isCustomElement: tag => tag.startsWith('x-')
          }
        }
      })
  }
}
```

Once you import ```@ututrust/web-components```, ```<x-utu-root>``` and ```<x-utu-recommendation>``` are ready to use.

```html
<template>
  <div class="offers">
    <x-utu-root ref="utu-root" api-key="[place your utu api key here]">
      <ul>
        <li v-for="offerId in offerIds" :key="offerId">
          <x-utu-recommendation :target-uuid="offerId" />
        </li>
      </ul>
    </x-utu-root>
  </div>
</template>

<script>
import "@ututrust/web-components";

export default {
  name: "Recommendations",
  data: function () {
    return {
      offerIds: ["e541df40-74b6-478e-a7da-7a9e52778700"]
    };
  },
};
</script>
```

