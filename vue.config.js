module.exports = {
  chainWebpack: config => {
    config.module
      .rule('vue')
      .use('vue-loader')
      .tap(options => {
        return {
          ...options,
          compilerOptions: {
            ...(options.compilerOptions || {}),
            isCustomElement: tag => tag.startsWith('x-')
          }
        }
      })
  }
}